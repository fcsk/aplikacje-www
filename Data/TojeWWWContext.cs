﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace TojeWWW.Models
{
    public class TojeWWWContext : DbContext
    {
        public TojeWWWContext (DbContextOptions<TojeWWWContext> options)
            : base(options)
        {
        }

        public DbSet<TojeWWW.Models.Movie> Movie { get; set; }
    }
}
